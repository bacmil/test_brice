@extends('welcome')

@section('content')

    <div class="card mt-5">
        <div class="card-title">
            Les Todos
        </div>
        <div class="card-body">
            <form action="{{route('todo.add')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-11">
                        <input class="form-control" name="title" type="text" required placeholder="Tache a faire">
                    </div>
                    <div class="col-1">
                        <button type="submit" class="btn btn-secondary">+</button>
                    </div>
                </div>

            </form>
            <div class="border-success">
                @foreach($items as $todo)
                    <div class="row mt-2">
                        <div class="col-2"><a href="{{route('todo.toogle',['id'=>$todo->id])}}"
                                              class="btn @if($todo->finished) btn-warning @else btn-success @endif">
                                @if($todo->finished)
                                    non terminer @else terminer @endif</a></div>
                        <div class="col-9"><span>{{$todo->name}} </span></div>
                            <div class="col-1"><a href="{{route('todo.delete',['id'=>$todo->id])}}" class="btn btn-danger"> Effacer </a></div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@stop