@extends('welcome')

@section('content')

    <div class="card mt-5">
        <div class="card-title">
            Gallerie
        </div>
        <div class="card-body">
            <form action="{{route('gallerie.add')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-9">
                        <input class="form-control" name="file" type="file" required placeholder="Tache a faire">
                        <input class="form-control mt-3" name="desc" type="text" required placeholder="Description">
                    </div>
                    <div class="col-3">
                        <button type="submit" class="btn btn-secondary mt-4">Ajouter une photo</button>
                    </div>
                </div>

            </form>
            <div class="border-success">

                <div class="row mt-2">
                    <div class="col-12">
                        <div id="nanogallery2"

                             data-nanogallery2='{
                            "thumbnailHeight":  150,
                            "thumbnailWidth":   150,
                            "itemsBaseURL":     ""
                            }'>
                            @foreach($items as $photos)
                                <a href="{{asset($photos->path)}}"
                                   data-ngThumb="{{asset($photos->path)}}"> {{$photos->description}} </a>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@stop