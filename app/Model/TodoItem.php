<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TodoItem extends Model
{
    protected $fillable = ['name'];
    public function __construct(array $attributes = [])
    {

        parent::__construct($attributes);
        $this->finished = false;
    }
}
