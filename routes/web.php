<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Model\Photo;
use App\Model\TodoItem;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Request;

Route::get('/', function () {
    return redirect('todo/index');
});

Route::group(['prefix' => 'todo', 'as' => 'todo.'], function () {
    Route::get('index', function () {

        $todos = TodoItem::all();
        return view('todo/index')->with('items', $todos);
    })->name('index');
    Route::post('add', function (Request $request) {

        $item = new TodoItem();
        $item->name = $request->get('title');
        $item->save();
        return redirect('todo/index');
    })->name('add');
    Route::get('toogle/{id}', function ($id) {

        $item = TodoItem::all()->find($id);
        $item->finished = !$item->finished;
        $item->save();
        return redirect('todo/index');
    })->name('toogle');
    Route::get('delete/{id}', function ($id) {

        $item = TodoItem::all()->find($id);
        $item->delete();
        return redirect('todo/index');
    })->name('delete');
});

Route::group(['prefix' => 'gallerie', 'as' => 'gallerie.'], function () {
    Route::get('index', function () {
        $photoa = Photo::all();
        return view('gallerie/index')->with('items',$photoa);
    })->name('index');
    Route::post('add', function (Request $request) {
        $photo = new Photo();
        $photo->description = $request->get('desc');
        $image = Input::file('file');
        $path = $image->store('public/image/');
        //dump($path);
        $photo->path = Storage::url($path);
        $photo->save();
        return redirect('gallerie/index');
    })->name('add');
});